# Crater morphology analysis tools


This project aims to provide python codes to analyse crater populations.

This project is still under development


***************
## Semi-automatic crater morphology extraction

Compute depth for each crater from a crater map and a DEM aligned on it. Provides a GUI.


CITE: [Semi-automated crater depth measurements, Sylvain Breton, Cathy Quantin-Nataf, Thomas Bodin, Damien Loizeau, Matthieu Volat and Loic Lozac’h, MethodX](https://doi.org/10.1016/j.mex.2019.08.007)



***************
## Crater dating

A Jupyter notebook course on crater dating can be found [here](./DatingDecouverteDatation.ipynb) (in French).

Implement Michael 2016 method do date planetary surfaces using a probabilistic poisson method. Chronology model and size frequency distribution models should be manually set to the wanted model (eg: moon/mars, Hartmann/Ivanov/Neukum...)

CITE: [Planetary surface dating from crater size-frequency distribution measurements: Poisson timing analysis", G.G. Michael, T. Kneissl, A. Neesemann, Icarus](https://doi.org/10.1016/j.icarus.2016.05.019)
 

***************
## Crater statistical morphology tools

Provide several tools to analyse, represent and model crater size and depth frequency distributions.
 
CITE: [Insight into Martian crater degradation history based on crater depth statistics, Sylvain Breton, Cathy Quantin-Nataf, Lu Pan, Lucia Mandon, Matthieu Volat, Icarus](https://doi.org/10.1016/j.icarus.2022.114898)


***************
## Instalation
In order to use those codes you need to install python 3.
Supplementary libraries are listed [here](./requirement.txt).


***************
## Contact
sylvain.breton@ac-reims.fr
